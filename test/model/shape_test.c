#include "model/shape.h"
#include "model/circle.h"
#include "model/rectangle.h"
#include "model/triangle.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h> 

void mock_draw_function(Shape *shape) {
    assert(number_of_sides_in_shape(shape) > 0);
    fprintf(stderr, "Hello! I am a mock polygon\n");
}

int should_delete_shape_with_no_errors() {
    Shape *shape = create_shape(12, mock_draw_function);
    delete_shape(shape);
    return 0;
}

int should_not_delete_null_shape() {
    delete_shape(NULL);
    return 0;
}

int should_not_delete_not_initialized_shape() {
    Shape *shape = NULL;
    delete_shape(shape);
    return 0;
}

int should_create_shape_with_properties() {
    Shape *shape = create_shape(22, mock_draw_function);
    assert(shape);
    assert(number_of_sides_in_shape(shape) == 22);
    delete_shape(shape);
    return 0;
}

int should_not_create_shape_without_draw_function() {
    Shape *shape = create_shape(45, NULL);
    assert(shape == NULL);
    return 0;
}

int should_draw_shape() {
    Shape *shape = create_shape(1, mock_draw_function);

    draw_shape(shape);

    delete_shape(shape);
    return 0;
}

int should_not_draw_null_shape() {
    draw_shape(NULL);
    return 0;
}

int should_have_circle_size() {
    Shape *circle = create_circle();
    assert(number_of_sides_in_shape(circle) == -1);
    delete_shape(circle);
    return 0;
}

int should_have_rectangle_size() {
    Shape *rectangle = create_rectangle();
    assert(number_of_sides_in_shape(rectangle) == 4);
    delete_shape(rectangle);
    return 0;
}

int should_have_triangle_size() {
    Shape *triangle = create_triangle();

    assert(number_of_sides_in_shape(triangle) == 3);
    delete_shape(triangle);
    return 0;
}

int main() {
    return should_delete_shape_with_no_errors()
        | should_not_delete_null_shape()
        | should_not_delete_not_initialized_shape()
        | should_create_shape_with_properties()
        | should_not_create_shape_without_draw_function()
        | should_draw_shape()
        | should_not_draw_null_shape()
        | should_have_circle_size()
        | should_have_rectangle_size()
        | should_have_triangle_size();
}