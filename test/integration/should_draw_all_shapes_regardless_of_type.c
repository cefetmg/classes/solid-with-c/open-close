#include <stdio.h>
#include <stdlib.h>
#include "model/shape.h"
#include "model/circle.h"
#include "model/triangle.h"
#include "model/rectangle.h"
#include "service/draw_shape_service.h"

void draw_square(Shape *_);

int should_draw_all_shapes_regardless_of_type() {
    Shape *shapes[] = {
        create_shape(4, draw_square),
        create_circle(),
        create_rectangle(),
        create_triangle()
    };

    draw_all_shapes(shapes, 4);

    int i;
    for (i = 0; i < 4; i++) {
        delete_shape(shapes[i]);
    }
    return 0;
}

int main() {
    return should_draw_all_shapes_regardless_of_type();
}

void draw_square(Shape *_) {
    printf("********\n");
    printf("*SQUARE*\n");
    printf("*      *\n");
    printf("********\n");
}