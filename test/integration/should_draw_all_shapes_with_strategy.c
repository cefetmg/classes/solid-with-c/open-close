#include <stdio.h>
#include <stdlib.h>
#include "model/shape.h"
#include "model/circle.h"
#include "model/triangle.h"
#include "model/rectangle.h"
#include "service/draw_shape_service.h"

void draw_square(Shape *_);

int strategy(Shape *shape);

int should_draw_all_shapes_given_Strategy() {
    Shape *shapes[] = {
        create_shape(4, draw_square),
        create_circle(),
        create_rectangle(),
        create_triangle()
    };
    draw_all_shapes_with_strategy(shapes, 4, strategy);
    
    int i;
    for (i = 0; i < 4; i++) {
        delete_shape(shapes[i]);
    }
    return 0;
}

int main() {
    return should_draw_all_shapes_given_Strategy();
}

int strategy(Shape *shape) {
    return number_of_sides_in_shape(shape) == 4;
}

void draw_square(Shape *_) {
    printf("*********\n");
    printf("*SQUARE *\n");
    printf("*2.0    *\n");
    printf("*********\n");
}