#include <stdio.h>
#include <stdlib.h>
#include "model/shape.h"
#include "model/circle.h"
#include "model/triangle.h"
#include "model/rectangle.h"
#include "service/draw_shape_service.h"


int should_draw_all_shapes() {
    Shape *shapes[] = {
        create_circle(),
        create_rectangle(),
        create_triangle()
    };

    draw_all_shapes(shapes, 3);

    int i;
    for (i = 0; i < 3; i++) {
        delete_shape(shapes[i]);
    }
    return 0;
}

int main() {
    return should_draw_all_shapes();
}