#include "service/draw_shape_service.h"
#include "model/shape.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int firstmock_calls = 0;
Shape *firstmock_expectedshape;

void firstmock_draw_mock_shape(Shape *shape) {
    assert(number_of_sides_in_shape(shape) == number_of_sides_in_shape(firstmock_expectedshape));
    firstmock_calls++;
    fprintf(stderr, "Printing the first mock shape\n");
}

int secondmock_calls = 0;
Shape *secondmock_expectedshape;

void secondmock_draw_mock_shape(Shape *shape) {
    assert(number_of_sides_in_shape(shape) == number_of_sides_in_shape(secondmock_expectedshape));
    secondmock_calls++;
    fprintf(stderr, "Printing the second mock shape\n");
}

int firstmock_strategycalls = 0;

int firstmock_strategy(Shape *shape) {
    firstmock_strategycalls++;
    return number_of_sides_in_shape(shape) == number_of_sides_in_shape(firstmock_expectedshape);
}

void _set_up() {
    firstmock_calls = 0;
    secondmock_calls = 0;
    firstmock_strategycalls = 0;
    firstmock_expectedshape = create_shape(-1, firstmock_draw_mock_shape);
    secondmock_expectedshape = create_shape(3, secondmock_draw_mock_shape);
}

void _tear_down() {
    firstmock_calls = 0;
    secondmock_calls = 0;
    firstmock_strategycalls = 0;
    delete_shape(firstmock_expectedshape);
    delete_shape(secondmock_expectedshape);
}

int should_call_first_mock_and_second_mock() {
    _set_up();
    const int size = 4;
    Shape *shapes[size];
    shapes[0] = create_shape(-1, firstmock_draw_mock_shape);
    shapes[1] = create_shape(3, secondmock_draw_mock_shape);
    shapes[2] = create_shape(3, secondmock_draw_mock_shape);
    shapes[3] = create_shape(-1, firstmock_draw_mock_shape);

    draw_all_shapes(shapes, size);

    int i;
    for (i = 0; i < size; i++) {
        delete_shape(shapes[i]);
    }
    assert(firstmock_calls == 2);
    assert(secondmock_calls == 2);
    _tear_down();
    return 0;
}

int should_call_mock_strategy() {
    _set_up();
    const int size = 5;
    Shape *shapes[size];
    delete_shape(firstmock_expectedshape);
    firstmock_expectedshape = create_shape(4, firstmock_draw_mock_shape);
    shapes[0] = create_shape(-1, firstmock_draw_mock_shape);
    shapes[1] = create_shape(-1, secondmock_draw_mock_shape);
    shapes[2] = create_shape(-1, firstmock_draw_mock_shape);
    shapes[3] = create_shape(-1, firstmock_draw_mock_shape);
    shapes[4] = create_shape(4, firstmock_draw_mock_shape);

    draw_all_shapes_with_strategy(shapes, size, firstmock_strategy);

    int i;
    for (i = 0; i < size; i++) {
        delete_shape(shapes[i]);
    }
    assert(firstmock_strategycalls == 5);
    assert(firstmock_calls == 1);
    _tear_down();
    return 0;
}

int main() {
    return should_call_first_mock_and_second_mock()
        | should_call_mock_strategy();
}