#include "service/draw_shape_service.h"
#include <stdio.h>
#include <stdlib.h>

void draw_all_shapes(Shape **shapes, int size) {
    int i;
    for(i = 0; i < size; i++) {
        draw_shape(shapes[i]);
    }
}

void draw_all_shapes_with_strategy(Shape **shapes, int size, int (*strategy)(Shape *)) {
    if (strategy == NULL) {
        return;
    }
    int i;
    for (i = 0; i < size; i++) {
        if (strategy(shapes[i])) {
            draw_shape(shapes[i]);
        }
    }
}