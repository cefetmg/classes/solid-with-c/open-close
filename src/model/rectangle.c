#include "model/rectangle.h"
#include <stdio.h>

void _draw_rectangle(Shape *rectangle) {
    printf("****************\n");
    printf("*              *\n");
    printf("*  RECTANGLE   *\n");
    printf("*              *\n");
    printf("****************\n");
}

Shape* create_rectangle() {
    return create_shape(4, _draw_rectangle);
}