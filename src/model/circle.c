#include "model/circle.h"
#include <stdio.h>

void _draw_circle(Shape *shape) {
    printf("   *****   \n");
    printf(" ***   *** \n");
    printf("*  CIRCLE *\n");
    printf(" ***   *** \n");
    printf("   *****   \n");
}

Shape* create_circle() {
    return create_shape(-1, _draw_circle);
}