#include "model/shape.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct shape {
    int m_number_of_sides;
    void (*m_drawing_function)(Shape *);
} Shape;

Shape * create_shape(const int number_of_sides, void (*drawing_function)(Shape *)) {
    if (drawing_function == NULL) {
        return NULL;
    }
    Shape *shape = malloc(sizeof(Shape));
    shape->m_drawing_function = drawing_function;
    shape->m_number_of_sides = number_of_sides;
    return shape;
}

int number_of_sides_in_shape(Shape *shape) {
    return shape->m_number_of_sides;
}

void delete_shape(Shape *shape) {
    if (shape == NULL) {
        return;
    }
    free(shape);
}

void draw_shape(Shape *shape) {
    if (shape == NULL) {
        return;
    }
    shape->m_drawing_function(shape);
}