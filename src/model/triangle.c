#include "model/triangle.h"
#include <stdio.h>

void _draw_triangle(Shape *triangle) {
    printf("  /\\  \n");
    printf(" /  \\  \n");
    printf("/____\\  \n");
}

Shape *create_triangle() {
    return create_shape(3, _draw_triangle);
}