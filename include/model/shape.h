
#ifndef SHAPE_MODEL_H
#define SHAPE_MODEL_H

typedef struct shape Shape;

Shape * create_shape(int, void (*)(Shape *));

int number_of_sides_in_shape(Shape *);

void delete_shape(Shape *);

void draw_shape(Shape *);

#endif