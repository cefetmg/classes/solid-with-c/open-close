#ifndef RECTANGLE_SHAPE_MODEL_H
#define RECTANGLE_SHAPE_MODEL_H

#include "model/shape.h"

Shape * create_rectangle();

#endif