#ifndef TRIANGLE_SHAPE_MODEL_H
#define TRIANGLE_SHAPE_MODEL_H

#include "model/shape.h"

Shape * create_triangle();

#endif