#ifndef CIRCLE_SHAPE_MODEL_H
#define CIRCLE_SHAPE_MODEL_H

#include "model/shape.h"

Shape * create_circle();

#endif