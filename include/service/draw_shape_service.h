#ifndef DRAW_SHAPE_SERVICE_H
#define DRAW_SHAPE_SERVICE_H

#include "model/shape.h"

void draw_all_shapes(Shape **, const int);

void draw_all_shapes_with_strategy(Shape **, const int, int (*strategy)(Shape *));

#endif
